package com.devsuperior.movieflix.resource;

import com.devsuperior.movieflix.dto.UserDTO;
import com.devsuperior.movieflix.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/users/profile")
public class UserResourse {

    @Autowired
    private UserService service;

    @GetMapping()
    public ResponseEntity<UserDTO> findById(){
        UserDTO dto = service.getUserLogged();
        return ResponseEntity.ok().body(dto);
    }

}
