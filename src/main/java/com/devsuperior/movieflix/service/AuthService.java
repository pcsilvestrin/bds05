package com.devsuperior.movieflix.service;

import com.devsuperior.movieflix.entities.User;
import com.devsuperior.movieflix.repositories.UserRepository;
import com.devsuperior.movieflix.service.exceptions.UnauthorizedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;

    @Transactional(readOnly = true)
    public User authenticated(){
        //Função que retorna o Usuário logado
        try {
            //Captura o nome do Usuário logado, que foi validado pelo Spring Security no caso o Email
            String userName = SecurityContextHolder.getContext().getAuthentication().getName();
            return userRepository.findByEmail(userName);
        } catch (Exception e){
            throw new UnauthorizedException("Invalid User");
        }
    }

    /*public void validateSelfOrAdmin(Long userId){
        //Valida se o ID do Usuário passado na Requisição é o mesmo do Usuário Logado (UserService)
        User user = authenticated();
        if (!user.getId().equals(userId) && !user.hasHole("ROLE_ADMIN") )  {
            throw new ForbiddenException("Access Denied");
        }
    }*/
}
