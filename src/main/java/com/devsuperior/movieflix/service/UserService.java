package com.devsuperior.movieflix.service;

import com.devsuperior.movieflix.dto.UserDTO;
import com.devsuperior.movieflix.entities.User;
import com.devsuperior.movieflix.repositories.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class UserService implements UserDetailsService {

    private static Logger logger = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private AuthService authService;


    @Autowired
    private UserRepository repository;

    @Transactional(readOnly = true)
    public UserDTO getUserLogged(){
        return new UserDTO(authService.authenticated());
    }

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        User user = repository.findByEmail(userName);
        if (user == null){
            logger.error("User not foud: "+userName);
            throw new UsernameNotFoundException("Email not Found");
        }

        logger.info("User found: "+userName);
        return user;
    }
}
